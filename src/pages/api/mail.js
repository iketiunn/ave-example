const { config } = require("firebase-functions");
const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid");
const mg = require("nodemailer-mailgun-transport");
const apiKey = config().email?.apikey || process.env.API_KEY;

const user = config().email?.user || process.env.EMAIL_USER;
const pass = config().email?.pass || process.env.EMAIL_PASS;
const to = config().email?.to || process.env.EMAIL_TO;
const auth = {
  auth: {
    api_key: apiKey,
    domain: "sandbox87832b6c251641a989f0e5c5902301f5.mailgun.org",
  },
};
const transporter = nodemailer.createTransport(mg(auth));

// const transporter = nodemailer.createTransport(
// sgTransport({
//   port: 587,
//   secure: false,
//   apiKey,
// })
// sgTransport({
//   port: 465,
//   secure: true,
//   apiKey,
// })
// sgTransport({
//   auth: {
//     apiKey,
//   },
//   port: 587,
//   secure: false,
//   rejectUnauthorized: false, //! ESSENTIAL! Fixes ERROR "Hostname/IP doesn't match certificate's altnames".
// })
// );

export default async function handler(req, res) {
  res.setHeader("Content-Type", "application/json");
  // Validation
  if (req.method !== "POST") {
    res.statusCode = 400;

    return res.send({ error: "Invalid method" });
  }
  const { subject, body } = req.body;
  if (!subject || !body) {
    res.statusCode = 400;

    return res.send({ error: "Missing subject or body!" });
  }

  // Main
  const mailOptions = {
    from: "ike.chang@sideeffect.dev",
    to,
    subject,
    html: `<p style="font-size: 16px;">${body}</p>
    `,
  };

  try {
    const info = await transporter.sendMail(mailOptions);
    console.log(info);

    res.statusCode = 201;
    return res.send({ result: "success" });
  } catch (err) {
    console.error(err);

    res.statusCode = 500;
    return res.send({ error: err.message });
  }
}
