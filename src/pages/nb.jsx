import App from "../components/App";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import React from "react";

export default function Nb() {
  const [a, setA] = React.useState();
  const [b, setB] = React.useState();
  const [c, setC] = React.useState();
  const [d, setD] = React.useState();

  return (
    <App>
      <div
        style={{
          margin: "0 auto",
          paddingTop: 120,
          textAlign: "center",
          width: "60%",
        }}
      >
        <h1>Numbers entry box</h1>
        <p>Use tab / shift+tab to move between boxes</p>
        <p>
          Only numbers (0-9), period (.), and backslash (/) allowed in the
          numbers box.
        </p>
        <TextField
          variant="outlined"
          autoFocus
          placeholder="1"
          inputProps={{ maxLength: 1, style: { textAlign: "center" } }}
          value={a}
          onInput={(e) => {
            if (!e.target.value.match(/(\d|\.|\/)/)) {
              e.target.value = "";
            }
            setA(e.target.value);
          }}
        />
        <TextField
          variant="outlined"
          placeholder="2"
          inputProps={{ maxLength: 1, style: { textAlign: "center" } }}
          value={b}
          onInput={(e) => {
            if (!e.target.value.match(/(\d|\.|\/)/)) {
              e.target.value = "";
            }
            setB(e.target.value);
          }}
        />
        <TextField
          variant="outlined"
          placeholder="3"
          inputProps={{ maxLength: 1, style: { textAlign: "center" } }}
          value={c}
          onInput={(e) => {
            if (!e.target.value.match(/(\d|\.|\/)/)) {
              e.target.value = "";
            }
            setC(e.target.value);
          }}
        />
        <TextField
          variant="outlined"
          placeholder="4"
          inputProps={{ maxLength: 1, style: { textAlign: "center" } }}
          value={d}
          onInput={(e) => {
            if (!e.target.value.match(/(\d|\.|\/)/)) {
              e.target.value = "";
            }
            setD(e.target.value);
          }}
        />
        {!!a && !!b && !!c && !!d && (
          <>
            <Button style={{ justifyContent: "end" }}>Press Enter ⏎</Button>
          </>
        )}
      </div>
    </App>
  );
}
