import App from "../components/App";

export default function Mail() {
  return (
    <App>
      <div style={{ margin: "0 auto", textAlign: "center" }}>
        <h1>Send Mail</h1>

        <h2>Hello John Doe</h2>
        <p>Fell free to ask anything!</p>
        <form action="api/mail" method="post">
          <label>Topic</label>
          <input
            type="text"
            name="subject"
            style={{ display: "block", margin: "0 auto" }}
          />
          <div>
            <label>Content</label>
            <textarea
              type="text"
              name="body"
              style={{
                display: "block",
                margin: "0 auto",
                height: 300,
                width: "30%",
              }}
            />
          </div>
          <button type="submit">Send</button>
        </form>
      </div>
    </App>
  );
}
