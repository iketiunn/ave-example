import * as React from "react";
import Link from "next/link";

const Header = ({ pathname }) => (
  <header>
    <Link href="/">
      <a className={pathname === "/" ? "is-active" : ""}>Home</a>
    </Link>{" "}
    <Link href="/mail">
      <a className={pathname === "/mail" ? "is-active" : ""}>Send Mail</a>
    </Link>{" "}
    <Link href="/nb">
      <a className={pathname === "/nb" ? "is-active" : ""}>Numbers entry box</a>
    </Link>
  </header>
);

export default Header;
